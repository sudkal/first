import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.css']
})
export class ProgressBarComponent implements OnInit {
default= "http://www.inkbrothers.com.au/uploads/3/0/2/3/3023284/people_orig.png";
 List = [{name:"Sudarsanam V",image:"/assets/Sudarsanam.jpg"},
 {name:"Kalyani S",image:"/assets/Kalyani.jpg"},
 {name:"Indu S",image:"/assets/Indu.jpg"},
 {name:"Suganya S",image:"/assets/Suganya.jpg"},
 {name:"Aadhrika PN",image:"/assets/Aadhrika.jpg"},
{name:"Srinivasan S",image:""},
{name:"Saranyan PN",image:""}]


sorted = "asc";
count =0;
constructor() {
this.List.sort(this.sortfn);
this.sorted = "asc";
this.count=this.List.length;
}

  sortfn = function(a, b){
    var keyA = a.name,
        keyB = b.name;
    // Compare the 2 names
    if(keyA < keyB) return -1;
    if(keyA > keyB) return 1;
    return 0;
}

sortRev = function(){
  this.List.reverse()
  this.sorted ="desc"
}
sort = function(){
  console.log("In Sort Function")
if (this.sorted == "asc"){
  this.sortRev();
}else{
  this.List.sort(this.sortfn);
  this.sorted ="asc"
}
}
  ngOnInit() {


  }


}

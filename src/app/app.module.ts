import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { ProgressBarComponent } from './progress-bar/progress-bar.component';
//import { ListPeopleComponent } from './list-people/list-people.component';

@NgModule({
  declarations: [
    ProgressBarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [ProgressBarComponent]

})
export class AppModule { }

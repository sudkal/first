#!/bin/bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

nvm use 7
node -v
npm -v
export NODE_ENV=production
if [ -d "first" ]; then
 serverStarted="True"
 echo "Directory Available fetching latest code"
 cd first 
 git pull origin master
else
 echo "Directory Does not Exist.Cloning the project"
  git clone https://gitlab.com/sudkal/first.git
  cd first
serverStarted="False"
fi

echo "Project fetch completed. Install node modules"
npm install
echo "Creating Static Files"
npm run build
echo $serverStarted
if [ "$serverStarted" = "False" ]
then
 echo "Starting Server"
 npm start
else
  echo "Restarting Server"
  npm restart
fi
echo "Server running"
